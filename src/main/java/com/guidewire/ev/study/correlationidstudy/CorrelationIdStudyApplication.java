package com.guidewire.ev.study.correlationidstudy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CorrelationIdStudyApplication {
	public static void main(String[] args) {
		SpringApplication.run(CorrelationIdStudyApplication.class, args);
	}
}
