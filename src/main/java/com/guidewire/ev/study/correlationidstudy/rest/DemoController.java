package com.guidewire.ev.study.correlationidstudy.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {
  private static Logger LOGGER = LoggerFactory.getLogger(DemoController.class);

  @GetMapping("/")
  public String home() {
    LOGGER.info("Hello from DemoController");
    return "Hello World";
  }
}